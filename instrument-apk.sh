#!/bin/bash

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat.mac
else
    echo "on non-MAC"
    . myconfig.dat
fi

die () { echo "This step went wrong"; exit 1; }

if [ "$verbose" = 1 ]; then
    exec 4>&2 3>&1
else
    exec 4>/dev/null 3>/dev/null
fi

if [ ! -z $1 -o -d $1/$1.jar.dir ]
then 
  FILE=$1
  bn=$(basename ${FILE%.*})
else
  echo "I cannont find the directory $1/$1.jar.dir"
  exit 1
fi

 if [ ! -z $3 -o -f $3 ] 
then
  toLog=$2
  toFlip=$3
  bnf=$(basename ${toLog%.*})
else
  echo "I need a json where you tell me what to instrument. Actually, I need two jsons: one for those points you just want to log, and another one for the paints to flip."
  exit 1
fi

if [ -z $4 ]
then
   num=${bnf#*-}
else
   num=$4
fi 

if [ -z $5 ]
then
   suffix=".jar.dir"
else
   suffix=$5
   echo " = From the directory $bn$suffix"
fi 


echo " = Let's create $bn-$num.apk"

if [ "$(uname -s)" == "Darwin" ]; then
  [ -e /usr/local/bin/d2j-dex2jar ] || brew install dex2jar
  [ -e /usr/local/bin/apktool ] || brew install apktool
fi

echo " = instrumenting $bn/$bn$suffix into $bn/$bn-$num"
cd conditionalFlipper/
$javacmd -Dfile.encoding=UTF-8 -classpath ./bin:./lib/asm-all-5.0.3.jar:./lib/commons-io-2.4.jar:./lib/org.json.jar instrumenter.asm.Instrumenter ../$bn/$bn$suffix ../$bn/$bn-$num ../$toFlip ../$toLog >&3 2>&4 || die
cd ..

echo " = repacking $bn-$num.apk"
./repack.sh $bn $bn-$num >&3 2>&4 || die
echo -e "\tmoving result to results/$bn-$num.apk"
mv $bn-$num.apk results/
