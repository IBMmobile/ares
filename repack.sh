#!/bin/bash

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat.mac
else
    echo "on non-MAC"
    . myconfig.dat
fi

bn=$1
newNumber=$2

if [ -e $bn/$newNumber/ ]
then

if [ "$(uname -s)" == "Darwin" ]; then
[ -e /usr/local/bin/d2j-dex2jar ] || brew install dex2jar
[ -e /usr/local/bin/apktool ] || brew install apktool
fi

cd $bn/$newNumber

echo " - include Runtime lib"
mkdir -p com/ibm/
cp $evasionRuntimeClass com/ibm/

echo " - pack the jar"
jar -cf $bn.jar .

echo " - convert $bn.jar back to dex format"
cd ..
rm ./classes.dex
if [ "$(uname -s)" == "Darwin" ]; then
    d2j-jar2dex -f $newNumber/$bn.jar -o ./classes.dex
else
    sh ../dex2jar-2.0/d2j-jar2dex.sh -f $newNumber/$bn.jar -o ./classes.dex
fi

echo " - repack to apk"
cd ..
if [ "$(uname -s)" == "Darwin" ]; then
    apktool b $bn -o $newNumber.apk
else
    ./apktool b $bn -o $newNumber.apk
fi

echo " - sign the package"
echo $signatureKey | $jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $keystore $newNumber.apk $alias_name

else
  echo "I cannot find $bn/$newNumber/"
  exit 1
fi
