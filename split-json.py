#!/usr/bin/python

import json
import sys
import itertools
import os
import re

def save(filename,klass,method,category,o):
  print filename,klass,method,category,o
  with open(filename, 'w') as outfile:
    json.dump({klass:{method:{category:o}}}, outfile)

infile = sys.argv[1]
basename = os.path.splitext(infile)[0]
i=0

# 000 is the empty json
save("%s-%.4i.json" % (basename,000), "", "", "", [])

with open(infile) as data_file:    
  classes = json.load(data_file)
  for klass, methods in classes.iteritems():
    for method,categories in methods.iteritems():
      conditionals = categories["conditional"] 
      for c in conditionals:
        i+=1
        save("%s-%.4i.json" % (basename,i), klass, method, "conditional", [c])
        with open(basename+'.idx', 'aw') as file:
           file.write("%.4i|%s %s:%s\n" % (i,re.sub('\.class$', '', klass),method,c))
