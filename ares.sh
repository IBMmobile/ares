#!/bin/bash

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat.mac
else
    echo "on non-MAC"
    . myconfig.dat
fi

die () { echo "This step went wrong"; exit 1; }

if [ "$verbose" = 1 ]; then
    exec 4>&2 3>&1
else
    exec 4>/dev/null 3>/dev/null
fi

if [ -z $1 ]
then
  echo "give me an apk to eat"
  exit 1
fi

if [ -z $2 ]
then
   emulator="emulator-5554"
else
   emulator="$2"
fi

FILE=$1
bn=$(basename ${FILE%.*})

if [ -f $1 ]
then

if [ -e $bn.wl  ]
then
   echo " = Resume workinglist  $bn.wl"
   case=$(head -n 1 $bn.wl)
else
   echo " = New workinglist $bn.wl"
   touch $bn.wl
echo " = installing canonnical apk $FILE"
./install-log-uninstall.sh $FILE $emulator

echo " = unpacking $FILE"
if [ -d $bn/$bn.jar.dir  ]
then
  echo -e "\tremoving previous directory $bn/$bn.jar.dir";
  rm -rf $bn/$bn.jar.dir
  rm $bn/$bn.jar
fi
./unpack.sh $FILE >&3 2>&4 || die
echo -e "\tunpacked in $bn/"

echo " = slicing analysis"
if [ -e results/$bn.json  ]
then
  echo -e "\tremoving previous result results/$bn.json and dependencyBuilder/graph/dat/$bn-*";
  rm results/$bn.json;
  rm dependencyBuilder/graph/dat/$bn-*
fi

cd dependencyBuilder/
$javacmd  -Dandroidjar=$androidjar -Dfile.encoding=UTF-8 -classpath ./bin:./lib/jgrapht-core-0.9.1.jar:./lib/jgraph-5.13.0.0.jar:./lib/org.json.jar:./lib:./lib/walautil.jar evasion.core.AnalysisEngine ../$bn/$bn.jar  || die
cd ..
echo -e "\tthe graph can be found in file://$(pwd)/dependencyBuilder/graph/index.html"

if [ -e results/$bn-0001.json  ]
then
  echo -e "\tremoving previous results results/$bn-*.json";
  rm results/$bn-*.json;
fi

if [ -e results/$bn.idx  ]
then
  echo -e "\tremoving previous results/$bn.idx and results/$bn.pending";
  rm results/$bn.idx
  rm results/$bn.pending
fi

echo " = spliting the cases from $FILE"
if [ -e results/$bn.json  ]
then
./split-json.py results/$bn.json >&3 2>&4 || die
else
echo "nothing to split"
echo '{"": {"": {"": []}}}' > results/$bn.json
echo '{"": {"": {"": []}}}' > results/$bn-0000.json
touch results/$bn.idx
fi

total=$(ls results/$bn-*.json | wc -l | tr -d '[[:space:]]')
echo -e "\t$total JSONs with EPCs generated"
cp results/$bn.idx results/$bn.pending

case="0000"
COUNTER=1

fi

./instrument-sleep-apk.sh $bn

while [ "" != "$case" ]
do
   START_TIME=$SECONDS

   echo " = instrumenting case: $case"
   ./instrument-apk.sh $bn results/$bn.json results/$bn-$case.json $case -sleep
   
   echo " = install and log case $case"
   ./install-log-uninstall.sh results/$bn-$case.apk $emulator
   
   echo " = analysing results/$bn-$case.log"
   ./log-analysis.sh results/$bn-$case.log results/$bn.idx $bn.wl $total

if [ "$(uname -s)" == "Darwin" ]; then
   sed -i "" "/^$case/d" $bn.wl
else
   sed -i "/^$case/d" $bn.wl
fi

   ELAPSED_TIME=$(($SECONDS - $START_TIME))

   echo "$COUNTER|$case" >> results/$bn.iterations
   
   echo "The case $case took  $ELAPSED_TIME seconds (iteration $COUNTER)"
   echo $ELAPSED_TIME > results/$bn-$case.time

   case=$(head -n 1 $bn.wl)
   last=$(ls results/$bn-*.json | wc -l | tr -d '[[:space:]]')
   total=$(ls -U results/$bn-*.json | tail -n1); total=${total#*-} ; total=${total%.*}
   let COUNTER=COUNTER+1 
done

exit 0

else
  echo "I cannot find $1"
  exit 1
fi

echo "Took $SECONDS seconds"
