/**
 * @author omer tripp and luciano bello
 * IBM Thomas J. Watson Research Center
 */
package evasion.spec;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import com.ibm.mobile.wala.util.Util;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.dalvik.classLoader.DexIRFactory;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.FieldReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.intset.MutableIntSet;
import com.ibm.wala.util.intset.MutableSparseIntSet;

public class Sources {
	
	private static final IntSet emptyset = MutableSparseIntSet.makeEmpty();
	
	private static final Collection<MethodReference> apis = new HashSet<>(Arrays.asList(new MethodReference[] {
		// MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getIntExtra", "(Ljava/lang/String;I)I"),
		// MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getIntArrayExtra", "(Ljava/lang/String;)[I"),
		// MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getAction", "()Ljava/lang/String;"),
		// MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getBooleanArrayExtra", "(Ljava/lang/String;)[Z"),
		// MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getBooleanExtra", "(Ljava/lang/String;Z)Z"),
		// MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getBundleExtra", "(Ljava/lang/String;)Landroid/os/Bundle;"),

		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Context"), "getPackageManager", "()Landroid/content/pm/PackageManager;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Context"), "getSharedPreferences", "(Ljava/lang/String;I)Landroid/content/SharedPreferences;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/telephony/TelephonyManager"), "getDeviceId", "()Ljava/lang/String;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/telephony/TelephonyManager"), "getSimSerialNumber", "()Ljava/lang/String;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/telephony/TelephonyManager"), "getSubscriberId", "()Ljava/lang/String;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/telephony/TelephonyManager"), "getCellLocation", "()Landroid/telephony/CellLocation;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/telephony/TelephonyManager"), "getLine1Number", "()Ljava/lang/String;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/telephony/TelephonyManager"), "getCellLocation", "()Landroid/telephony/CellLocation;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/telephony/TelephonyManager"), "getNeighboringCellInfo", "()Ljava/util/List;"),
		
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/lang/System"), "loadLibrary", "(Ljava/lang/String;)V"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/lang/System"), "currentTimeMillis", "()J"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/SystemClock"), "uptimeMillis", "()J"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/SystemClock"), "elapsedRealtime", "()J"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/SystemClock"), "elapsedRealtimeNanos", "()J"),
		
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/location/LocationManager"), "getLastKnownLocation", "(Ljava/lang/String;)Landroid/location/Location;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/location/LocationManager"), "isProviderEnabled", "(Ljava/lang/String;)Z"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/location/LocationManager"), "requestLocationUpdates", "(Ljava/lang/String;JFLandroid/location/LocationListener;)V"),
		
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getIntExtra", "(Ljava/lang/String;I)I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getIntArrayExtra", "(Ljava/lang/String;)[I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getAction", "()Ljava/lang/String;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getBooleanArrayExtra", "(Ljava/lang/String;)[Z"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getBooleanExtra", "(Ljava/lang/String;Z)Z"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Intent"), "getBundleExtra", "(Ljava/lang/String;)Landroid/os/Bundle;"),
		
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/lang/System"), "nanoTime", "()J"),
		/* MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getHours", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getDate", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getMinutes", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getDate", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getMonth", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getSeconds", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getYear", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "getTime", "()J"),*/
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar"), "getTime", "()Ljava/util/Date;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar"), "getTimeInMillis", "()J"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Date"), "<init>", "()V"),
		// MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/io/File"), "exists", "()Z"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/ConnectivityManager"), "getActiveNetworkInfo", "()Landroid/net/NetworkInfo;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/ConnectivityManager"), "getNetworkInfo", "()Landroid/net/NetworkInfo;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/ConnectivityManager"), "getAllNetworkInfo", "()[Landroid/net/NetworkInfo;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/ConnectivityManager"), "getAllNetworks", "()[Landroid/net/NetworkInfo;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/net/NetworkInterface"), "getInetAddresses", "()Ljava/util/Enumeration;"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/wifi/WifiInfo"), "getIpAddress", "()I"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/NetworkInfo"), "isConnectedOrConnecting", "()Z"),
		
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/wifi/WifiManager"), "isWifiEnabled", "()Z"),
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/net/wifi/WifiManager"), "getConnectionInfo", "()Landroid/net/wifi/WifiInfo;"),
		
		MethodReference.findOrCreate(TypeReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/Context"), "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;")
	}));
	private static final Collection<String> sconstants = Arrays.asList(new String[] { 
		"/proc/net/tcp",
		"adb_enabled", // "Landroid/provider/Settings/Global", "ADB_ENABLED", "Ljava/lang/String;"
		"debug_app",  // "Landroid/provider/Settings/Global", "DEBUG_APP", "Ljava/lang/String;"
		"development_settings_enabled", // "Landroid/provider/Settings/Global", "DEVELOPMENT_SETTINGS_ENABLED", "Ljava/lang/String;"
		"device_provisioned", // "Landroid/provider/Settings/Global", "DEVICE_PROVISIONED", "Ljava/lang/String;"
		"http_proxy", // "Landroid/provider/Settings/Global", "HTTP_PROXY", "Ljava/lang/String;"
		"wait_for_debugger", // "Landroid/provider/Settings/Global", "WAIT_FOR_DEBUGGER", "Ljava/lang/String;"
		//"0F02000A",
		//"\\W+",
		//"com.android.vending",
		"samsung",
		//"com.android.providers.downloadsmanager",
		//"unknown",
		//"generic",
		//"sdk",
		"89014103211118510720", // getSimSerialNumber();
		"310260000000000",      // getSubscriberId();
		"15552175049",          // getVoiceMailNumber();
		"15555215554", // getLine1Number();
		"310260", //getNetworkOperator();
		"android.net.conn.CONNECTIVITY_CHANGE",
		"android.intent.action.BOOT_COMPLETED",
		"android.provider.Telephony.SMS_RECEIVED",
		"android.intent.extra.PHONE_NUMBER",
		"android.intent.action.NEW_OUTGOING_CALL",
		"android.intent.action.RUN",
		"android.intent.action.DELETE",
		"android.intent.action.VIEW",
		"android.intent.action.PHONE_STATE",
		"android.intent.action.PACKAGE_ADDED",
		"android.intent.action.PACKAGE_REMOVED",
        "android.intent.action.PACKAGE_CHANGED",
        "android.intent.action.PACKAGE_REPLACED",
        "android.intent.action.PACKAGE_RESTARTED",
        "android.intent.action.PACKAGE_INSTALL"
	});
	private static final Collection<Integer> iconstants = Arrays.asList(new Integer[] { 
	/*	5555,
		5585 */ 
	});
	private static final Collection<FieldReference> flds = Arrays.asList(new FieldReference[] {
		// FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/BatteryManager", "BATTERY_STATUS_CHARGING", "I"),
		// FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/BatteryManager", "BATTERY_STATUS_FULL", "I"),
			
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/BatteryManager", "EXTRA_STATUS", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "MANUFACTURER", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "BRAND", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build/VERSION", "SDK", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "HARDWARE", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "FINGERPRINT", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "MODEL", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "PRODUCT", "Ljava/lang/String;"), 
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "SERIAL", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "USER", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "CPU_ABI", "Ljava/lang/String;"), 
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "CPU_ABI2", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "BOARD", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "DEVICE", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "HOST", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "ID", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "DISPLAY", "Ljava/lang/String;"), 
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "TYPE", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/Build", "TAGS", "Ljava/lang/String;"),
		//FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/BatteryManager", "BATTERY_STATUS_CHARGING", "I"),
		//FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/BatteryManager", "BATTERY_STATUS_FULL", "I"),
		// FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/os/BatteryManager", "EXTRA_LEVEL", "Ljava/lang/String;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/content/pm/PackageInfo", "signatures", "[Landroid/content/pm/Signature;"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Landroid/hardware/SensorEvent", "values", "[F"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar", "HOUR_OF_DAY", "I"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar", "DATE", "I"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar", "MONTH", "I"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar", "YEAR", "I"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar", "DAY_OF_WEEK", "I"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar", "SECOND", "I"),
		FieldReference.findOrCreate(ClassLoaderReference.Application, "Ljava/util/Calendar", "MINUTE", "I")
	});
	
	private final IClassHierarchy cha;
	public Sources(IClassHierarchy cha) {
		this.cha = cha;
	}
	
	public boolean addCustomSource(MethodReference mr) {
		return apis.add(mr);
	}

	public boolean hasCustomSource(MethodReference mr) {
		return apis.contains(mr);
	}
	
	public IntSet get(IMethod mthd) {
		MutableIntSet result = MutableSparseIntSet.makeEmpty();
		if (mthd.isAbstract() || mthd.isNative())
			return emptyset;
		IR ir = new DexIRFactory().makeIR(mthd, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());
		if (ir == null)
			return result;
		for (int i=0; i<ir.getInstructions().length; ++i) {
			SSAInstruction instruction = ir.getInstructions()[i];
			if (instruction == null)
				continue;
//			DefUse du = new DefUse(ir);
			IntSet r = process(ir, instruction);
			result.addAll(r);
		}
		return result;
		
	}

	private IntSet process(IR ir, SSAInstruction instruction) {
		MutableIntSet result = MutableSparseIntSet.makeEmpty(); 
		IntSet i1 = checkForSConstants(ir, instruction);	
		result.addAll(i1);
		IntSet i2 = checkForIConstants(ir, instruction);
		result.addAll(i2);
		IntSet i3 = checkForAPIs(ir, instruction);
		result.addAll(i3);
		IntSet i4 = checkForFields(ir, instruction);
		result.addAll(i4);
		return result;
	}

	private IntSet checkForSConstants(IR ir, SSAInstruction instruction) {
		MutableIntSet result = MutableSparseIntSet.makeEmpty();
		SymbolTable sb = ir.getSymbolTable();
		for (int i=0; i<instruction.getNumberOfUses(); ++i) {
			int use = instruction.getUse(i);
			if (sb.isStringConstant(use)) {
				String value = sb.getStringValue(use);
				if (sconstants.contains(value))
					result.add(use);
			}
		}
		return result;
	}
	
	private IntSet checkForIConstants(IR ir, SSAInstruction instruction) {
		MutableIntSet result = MutableSparseIntSet.makeEmpty();
		SymbolTable sb = ir.getSymbolTable();
		for (int i=0; i<instruction.getNumberOfUses(); ++i) {
			int use = instruction.getUse(i);
			if (sb.isIntegerConstant(use)) {
				int value = sb.getIntValue(use);
				if (iconstants.contains(value))
					result.add(use);
			}
		}
		return result;
	}
	
	private IntSet checkForAPIs(IR ir, SSAInstruction instruction) {
		if (instruction instanceof SSAAbstractInvokeInstruction) {
			SSAAbstractInvokeInstruction invoke = (SSAAbstractInvokeInstruction) instruction;
			MethodReference tgtref = invoke.getDeclaredTarget();
			IMethod method = cha.resolveMethod(tgtref);
			int def = instruction.hasDef() ? instruction.getDef() : -1;
			if (method != null && def!=-1) {
				//We want to exclude the native methods that are in the library
				//and consider as source only the ones that are in the application
				//since they might be used to leak information without exposing the code
				MethodReference realtarget = Util.resolveMethodCall(invoke, cha);
				IClass clas = cha.lookupClass(realtarget.getDeclaringClass());
				if(method.isNative() && ! clas.getClassLoader().getReference().equals(ClassLoaderReference.Primordial))
					return MutableSparseIntSet.singleton(instruction.getDef());
			if (def!=-1 && apis.contains(tgtref))
				return MutableSparseIntSet.singleton(instruction.getDef());
			else if (apis.contains(tgtref) && tgtref.getName().toString().equals("<init>"))
				return MutableSparseIntSet.singleton(instruction.getUse(0));
			}
		}
		return emptyset;
	}
	
	private IntSet checkForFields(IR ir, SSAInstruction instruction) {
		if (instruction instanceof SSAGetInstruction) {
			SSAGetInstruction getfld = (SSAGetInstruction) instruction;
			FieldReference fldref = getfld.getDeclaredField();
			if (flds.contains(fldref))
				return MutableSparseIntSet.singleton(instruction.getDef());
		}
		return emptyset;
	}
}
