package evasion.observer;

/**
 * 
 * @author omertripp
 *
 */
public interface IObserver {
	void onNewGraph(String mname);
	void onVertexAdded(Integer vertex);
	void onEdgeAdded(Integer src, Integer tgt, String label);
}
