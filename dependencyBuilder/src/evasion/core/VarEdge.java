/**
 * 
 */
package evasion.core;

/**
 * @author omertripp
 *
 */
public class VarEdge {

	public final int src, tgt, srcII, tgtII;
	
	public VarEdge(int src, int tgt, int srcII, int tgtII) {
		this.src = src;
		this.tgt = tgt;
		this.srcII = srcII;
		this.tgtII = tgtII;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + src;
		result = prime * result + srcII;
		result = prime * result + tgt;
		result = prime * result + tgtII;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VarEdge other = (VarEdge) obj;
		if (src != other.src)
			return false;
		if (srcII != other.srcII)
			return false;
		if (tgt != other.tgt)
			return false;
		if (tgtII != other.tgtII)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%d => %d", src, tgt);
	}
}
