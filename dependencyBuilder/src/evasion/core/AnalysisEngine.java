package evasion.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.jar.JarFile;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DirectedPseudograph;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ibm.wala.cfg.cdg.ControlDependenceGraph;
import com.ibm.wala.classLoader.IBytecodeMethod;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.dalvik.classLoader.DexIMethod;
import com.ibm.wala.dalvik.classLoader.DexIRFactory;
import com.ibm.wala.dalvik.util.AndroidAnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.impl.Everywhere;
import com.ibm.wala.ipa.callgraph.impl.FakeRootMethod;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.shrikeBT.IInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.ISSABasicBlock;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAAbstractThrowInstruction;
import com.ibm.wala.ssa.SSAAbstractUnaryInstruction;
import com.ibm.wala.ssa.SSAArrayLengthInstruction;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSAArrayReferenceInstruction;
import com.ibm.wala.ssa.SSABinaryOpInstruction;
import com.ibm.wala.ssa.SSACheckCastInstruction;
import com.ibm.wala.ssa.SSAComparisonInstruction;
import com.ibm.wala.ssa.SSAConditionalBranchInstruction;
import com.ibm.wala.ssa.SSAConversionInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;
import com.ibm.wala.ssa.SSAGetCaughtExceptionInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAGotoInstruction;
import com.ibm.wala.ssa.SSAInstanceofInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInstructionFactory;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSALoadMetadataInstruction;
import com.ibm.wala.ssa.SSAMonitorInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAOptions;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.ssa.SSASwitchInstruction;
import com.ibm.wala.ssa.SSAUnaryOpInstruction;
import com.ibm.wala.ssa.SymbolTable;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.collections.HashSetFactory;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.intset.IntIterator;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.intset.MutableIntSet;
import com.ibm.wala.util.intset.MutableSparseIntSet;

import evasion.observer.IObserver;
import evasion.spec.Sources;

public class AnalysisEngine {
	
	private static class InstructionArrayWrapper {
		
		private final com.ibm.wala.dalvik.dex.instructions.Instruction[] dexInstructions;
		private final IInstruction[] shrikeInstructions;
		
		public InstructionArrayWrapper(IInstruction[] shrikeInstructions) {
			this.shrikeInstructions = shrikeInstructions;
			this.dexInstructions = null;
		}
		
		public InstructionArrayWrapper(com.ibm.wala.dalvik.dex.instructions.Instruction[] dexInstructions) {
			this.shrikeInstructions = null;
			this.dexInstructions = dexInstructions;
		}
		
		public int length() {
			if (shrikeInstructions == null)
				return dexInstructions.length;
			else
				return shrikeInstructions.length;
		}
		
		public String toString(int idx) {
			if (shrikeInstructions == null)
				return dexInstructions[idx].toString();
			else
				return shrikeInstructions[idx].toString();
		}
		
		public int getOpcode(int idx) {
			if (shrikeInstructions == null)
				return dexInstructions[idx].getOpcode().flags;
			else
				return ((Instruction) shrikeInstructions[idx]).getOpcode();
		}
	}
	
	private static class SSAInstructionOrConstantWrapper {
		final SSAInstruction inst;
		final Object constant;
		final int bytecodeIndex;
		SSAInstructionOrConstantWrapper(SSAInstruction inst) {
			this.inst = inst;
			this.constant=null;
			bytecodeIndex=-1;
		}
		SSAInstructionOrConstantWrapper(SSAInstruction inst, int bytecodeIndex) {
			this.inst = inst;
			this.bytecodeIndex = bytecodeIndex;
			constant=null;
		}
		SSAInstructionOrConstantWrapper(Object constant) {
			this.inst = null;
			this.constant=constant;
			this.bytecodeIndex=-1;
		}
		public int getBytecodeIndex() {
			return bytecodeIndex;
		}
		public SSAInstruction getInst() {
			return inst;
		}
		public Object getConstant() {
			return constant;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((constant == null) ? 0 : constant.hashCode());
			result = prime * result + ((inst == null) ? 0 : inst.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SSAInstructionOrConstantWrapper other = (SSAInstructionOrConstantWrapper) obj;
			if (constant == null) {
				if (other.constant != null)
					return false;
			} else if (!constant.equals(other.constant))
				return false;
			if (inst == null) {
				if (other.inst != null)
					return false;
			} else if (!inst.equals(other.inst))
				return false;
			return true;
		}
		@Override
		public String toString() {
			return inst != null ? inst.toString() : constant.toString();
		}
		
		
	}
	
	private static int edgecounter = 0;
	
	private static final int STOP = -1;
	private static final int SOURCE = -2;

	private final static Set<IObserver> observers = HashSetFactory.make();

	private static final boolean conditions_as_sinks = false;
	public static boolean register(IObserver observer) {
		return observers.add(observer);
	}
	
	public static void main(String[] args) throws ClassHierarchyException, IllegalArgumentException, IOException, CancelException, InvalidClassFileException {
		com.ibm.mobile.wala.util.Util.EXCLUSIONS = "./dat/exclusions.txt";
		String jarpath = args[0];
		analyze(new File(jarpath));
		
	}
	
	public static void analyze(String jarpath) throws InvalidClassFileException {
		analyze(new File(jarpath));
	}
	
	 
	
	public static void analyze(File jarfile) throws InvalidClassFileException {
		try {
			IClassHierarchy cha = makeCHA(jarfile.getAbsolutePath());
			// ScandroidEngine engine = new
			// ScandroidEngine(jarfile.getAbsolutePath(), true);
			// CallGraph cg = engine.buildDefaultCallGraph();

			Sources s = new Sources(cha);
			List<String> filenames = new ArrayList<String>();
			
			BufferedWriter dep = new BufferedWriter(new FileWriter("../results/"+FilenameUtils.removeExtension(jarfile.getName())+".dep", true));
			boolean done = false;
			while (!done) {
				done = true;
				L: for (IClass klass : cha) {
					for (IMethod mthd : klass.getDeclaredMethods()) {
						if (s.hasCustomSource(mthd.getReference()))
							continue L;
						IntSet srcs = s.get(mthd);
						if (srcs.size() > 0) {
							String fileout = FilenameUtils
									.removeExtension(jarfile.getName());
							DirectedGraph<Integer, Pair<Integer, SSAInstructionOrConstantWrapper>> graph = doIntraprocAnalysis(
									mthd, srcs, fileout);
							
							Set<Pair<Integer, SSAInstructionOrConstantWrapper>> inedges = graph
									.incomingEdgesOf(STOP);
							for (Pair<Integer, SSAInstructionOrConstantWrapper> inedge : inedges) {
								SSAInstruction instruction = inedge.snd.inst;
								if (instruction instanceof SSAReturnInstruction) {
									s.addCustomSource(mthd.getReference());
									done = false; // To avoid custom sources, comment out this line...
									// break L;

									IR ir = new DexIRFactory().makeIR(mthd, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());
									for (IntIterator iit = srcs.intIterator(); iit.hasNext(); ) {
										int v = iit.next();
										
										SSAInstruction ass = new DefUse(ir).getDef(v);
										dep.append(mthd.getReference()+ " <- ");
										if (ass == null) {
											SymbolTable symtable = ir.getSymbolTable();
											dep.append(symtable.getConstantValue(v).toString());
										} else
											dep.append(ass.toString());
									}
									dep.newLine();
									
								}
							}

							DirectedGraph<SSAInstructionOrConstantWrapper, VarEdge> flipped = flip(graph);
							// System.out.println(graph);
							// System.out.println(flipped);

							String filename = fileout
									+ "-"
									+ mthd.getDeclaringClass().getName()
											.toString().substring(1)
											.replace('/', '.')
									+ "."
									+ mthd.getName()
									+ '-'
									+ mthd.getDescriptor().toString()
											.hashCode() + "." + "json";
							JSONObject json = toJSONObject_flipped(flipped);
							// JSONObject json = toJSONObject(graph);

							dumpJsonToFile(filename, json);
							filenames.add(filename);
							addFiles(filenames);
						}
					}
				}
			}
			dep.close();
		} catch (IOException | JSONException | ClassHierarchyException _) {
		}
	}

	private static IClassHierarchy makeCHA(String jar) throws IOException, ClassHierarchyException {
		String extension = jar.substring(jar.lastIndexOf(".") + 1);
		AnalysisScope scope = null;
		if ((extension.equals("jar")) || (extension.equals("apk"))) {
			scope = AndroidAnalysisScope.setUpAndroidAnalysisScope(
					com.ibm.mobile.wala.util.Util.ANDROID_JAR, jar,
					com.ibm.mobile.wala.util.Util.EXCLUSIONS);
		} else if (extension.equals("txt")) {
			scope = AnalysisScopeReader.readJavaScope(jar, new File(
					CallGraphTestUtil.REGRESSION_EXCLUSIONS), ClassLoader
					.getSystemClassLoader());
			scope.addToScope(ClassLoaderReference.Primordial, new JarFile(
					com.ibm.mobile.wala.util.Util.ANDROID_JAR));
		}
		IClassHierarchy cha = ClassHierarchy.make(scope);
		return cha;
	}

	private static DirectedGraph<SSAInstructionOrConstantWrapper, VarEdge> flip(
			DirectedGraph<Integer, Pair<Integer, SSAInstructionOrConstantWrapper>> graph) {
		DirectedGraph<SSAInstructionOrConstantWrapper, VarEdge> result = new DirectedPseudograph<SSAInstructionOrConstantWrapper, VarEdge>(VarEdge.class);
		for (Pair<Integer, SSAInstructionOrConstantWrapper> edge : graph.edgeSet()) {
			result.addVertex(edge.snd);
			Integer es = graph.getEdgeSource(edge);
			Integer et = graph.getEdgeTarget(edge);
			for (Pair<Integer, SSAInstructionOrConstantWrapper> eout : graph.outgoingEdgesOf(et)) {
				SSAInstructionOrConstantWrapper tgt = eout.snd;
				result.addVertex(tgt);
				int firstindex = edge.snd.getInst()!=null ? edge.snd.getInst().iindex : -1;
				int secondindex = tgt.getInst()!=null ? tgt.getInst().iindex : -1;
				VarEdge lbl = new VarEdge(es, et, firstindex, secondindex);
				boolean hasEdges = !result.getAllEdges(edge.snd, tgt).isEmpty();
				boolean edgeAdded = result.addEdge(edge.snd, tgt, lbl);
				assert (hasEdges || edgeAdded); 
				//if(! edgeAdded) //We MUST know if an edge is not added to the graph!
					// System.out.println("Edge from "+edge.snd+" to "+tgt+" not added");
			}
		}
		return result;
	}

	public static void addFiles(List<String> filenames) throws JSONException, IOException {
        JSONObject newJson = new JSONObject();
        HashSet<String> filenamesSet = new HashSet<String>(filenames);
		JSONArray files = new JSONArray(filenamesSet);
		try {
			InputStream is = new FileInputStream("graph/dat/files.json");
			String jsonTxt = IOUtils.toString(is);
            JSONObject json = new JSONObject(jsonTxt);
            JSONArray oldFiles = json.getJSONArray("files");
            for (int i=0; i<oldFiles.length(); ++i) {
            	if (!filenamesSet.contains(oldFiles.get(i))){
            		files.put(oldFiles.get(i));
            	}
            }
		} catch (FileNotFoundException e) {}
		newJson.put("files", files);
    	dumpJsonToFile("files.json",newJson);
	}
	
	private static void dumpJsonToFile(String filename, JSONObject json) throws IOException {
	    FileWriter file = new FileWriter("graph/dat/"+filename);
		file.write(json.toString());
        file.flush();
        file.close();
	}
		
	public static JSONObject toJSONObject_flipped(DirectedGraph<SSAInstructionOrConstantWrapper, VarEdge> graph) throws JSONException {
		JSONObject result = new JSONObject();
		JSONArray e = new JSONArray();
		JSONArray n = new JSONArray();
		result.put("edges", e); 
		result.put("nodes", n);
		for(VarEdge ed : graph.edgeSet()) {
			JSONObject edge = new JSONObject();
			edge.put("label", ed.tgt);
			SSAInstructionOrConstantWrapper from = graph.getEdgeSource(ed);
			SSAInstructionOrConstantWrapper to = graph.getEdgeTarget(ed);
			if (from.inst != null)
				edge.put("from", from.inst.iindex+": "+from);
//			try { edge.put("from", from.inst.iindex+": "+from);}
//			catch (NullPointerException e1){edge.put("from", from.inst+": "+from);}
			if (to.inst != null)
				edge.put("to",  to.inst.iindex+": "+to);
			result.accumulate("edges", edge);
		}
		
		for(SSAInstructionOrConstantWrapper in : graph.vertexSet()) {
			JSONObject node = new JSONObject();
			SSAInstruction ins = in.getInst();
			if(ins==null) {
				node.put("label", in.getConstant());
			}
			else if (ins instanceof SSAInvokeInstruction) {
				SSAInvokeInstruction st = (SSAInvokeInstruction) ins;
				node.put("label", st.getDeclaredTarget().getName());
				node.put("title", st.getDeclaredTarget().getDeclaringClass().getName());
				node.put("type", "invokevirtual");
			} else if (ins instanceof SSAConditionalBranchInstruction) {
				SSAConditionalBranchInstruction st = (SSAConditionalBranchInstruction) ins;
				node.put("label", st.getOperator());
				node.put("type", "branching");
				node.put("title", "bytecodeIndex: " + in.getBytecodeIndex());
			} else if (ins instanceof SSAReturnInstruction) {
				node.put("title", "return " + ins.getUse(0));
				node.put("label", "return");
				node.put("type", "return");
			} else if (ins instanceof SSAGetInstruction) {
				SSAGetInstruction get = (SSAGetInstruction) ins;
				node.put("label", get.getDeclaredField().getName());
				node.put("title", get.getDeclaredField().getSignature());
				node.put("type", "getfield");
			} else if (ins instanceof SSAArrayLoadInstruction) {
				SSAArrayLoadInstruction arrload = (SSAArrayLoadInstruction) ins;
				node.put("title", arrload.getArrayRef()+"["+arrload.getIndex()+"]");
				node.put("type", "arrayload");
			} else if(ins instanceof SSAPhiInstruction) {
				SSAPhiInstruction phi = (SSAPhiInstruction) ins;
				node.put("title", phi.toString());
				node.put("type", "phi");
			} else if(ins instanceof SSACheckCastInstruction) {
				SSACheckCastInstruction checkCast = (SSACheckCastInstruction) ins;
				node.put("title", checkCast.toString());
				node.put("type", "checkCast");
			} else if(ins instanceof SSABinaryOpInstruction) {
				SSABinaryOpInstruction biop = (SSABinaryOpInstruction) ins;
				node.put("label", biop.getOperator());
				node.put("title", biop.toString());
				node.put("type", "binaryop");
			} else if(ins instanceof SSAConversionInstruction) {
				SSAConversionInstruction convert = (SSAConversionInstruction) ins;
				node.put("title", "convert "+convert.getToType());
				node.put("type", "convert");
			} else if(ins instanceof SSAArrayLengthInstruction) {
				SSAArrayLengthInstruction arraylength = (SSAArrayLengthInstruction) ins;
				node.put("title", "arraylength "+arraylength.toString());
				node.put("type", "arraylength");
			} else if(ins instanceof SSAInstanceofInstruction) {
				SSAInstanceofInstruction instanceofins = (SSAInstanceofInstruction) ins;
				node.put("title", "instanceof "+instanceofins.getCheckedType());
				node.put("type", "instanceof");
			} else if(ins instanceof SSANewInstruction) {
				SSANewInstruction newst = (SSANewInstruction) ins;
				node.put("title", "new "+newst.getConcreteType());
				node.put("type", "new");
			} else if(ins instanceof SSAComparisonInstruction) {
				SSAComparisonInstruction cmpst = (SSAComparisonInstruction) ins;
				node.put("title", "cmp "+cmpst.getOperator());
				node.put("type", "cmp");
			} else if(ins instanceof SSAUnaryOpInstruction) {
				SSAUnaryOpInstruction uniop = (SSAUnaryOpInstruction) ins;
				node.put("label", uniop.getOpcode());
				node.put("title", uniop.toString());
				node.put("type", "unaryop");
			}
			else if(ins instanceof SSAGetCaughtExceptionInstruction) {
				SSAGetCaughtExceptionInstruction excop = (SSAGetCaughtExceptionInstruction) ins;
				node.put("label", "catch");
				node.put("title", "catch "+excop.getExceptionTypes());
				node.put("type", "catch");
				
			}
			else if(ins instanceof SSAInstructionFactory) {
				node.put("label", "InstructionFactory");
				node.put("type", "InstructionFactory");
				
			}
			else if(ins instanceof SSALoadMetadataInstruction) {
				SSALoadMetadataInstruction loadMetadata = (SSALoadMetadataInstruction) ins;
				node.put("label", "LoadMetadataInstruction");
				node.put("title", loadMetadata.getType());
				node.put("type", "loadMetadata");
			}
			else{
				System.out.println("Error: "+ins.getClass().getName());
				throw new Error(); };
			node.put("id", ins==null?in.getConstant() : ins.iindex+": "+ins);
			result.accumulate("nodes", node);
		}
		return result;
	}
	
	public static JSONObject toJSONObject(DirectedGraph<Integer,Pair<Integer, SSAInstructionOrConstantWrapper>> graph) throws JSONException {
		JSONObject result = new JSONObject();
		JSONArray e = new JSONArray();
		JSONArray n = new JSONArray();
		result.put("edges", e); 
		result.put("nodes", n);
		
		for(Pair<Integer, SSAInstructionOrConstantWrapper> in : graph.edgeSet()) {
			JSONObject edge = new JSONObject();
			SSAInstruction ins = in.snd.getInst();
			if(ins==null) {
				edge.put("label", in.snd.getConstant());
				edge.put("title", in.snd.getConstant());
				edge.put("type", "constant");
			} else if (ins instanceof SSAInvokeInstruction) {
				SSAInvokeInstruction st = (SSAInvokeInstruction) ins;
				edge.put("label", st.getDeclaredTarget().getName());
				edge.put("title", st.getDeclaredTarget().getDeclaringClass());
				edge.put("type", "invokevirtual");
			} else if (ins instanceof SSAConditionalBranchInstruction) {
				SSAConditionalBranchInstruction st = (SSAConditionalBranchInstruction) ins;
				edge.put("title", st.getOperator());
				edge.put("type", "branching");
			} else if (ins instanceof SSAReturnInstruction) {
				edge.put("title", " return " + ins.getUse(0));
				edge.put("bytecode index", in.snd.bytecodeIndex);
				edge.put("type", "return");
			} else if (ins instanceof SSAGetInstruction) {
				SSAGetInstruction st = (SSAGetInstruction) ins;
				edge.put("title", st.getDeclaredField().getName());
				edge.put("type", "get");
			} else throw new Error();
			edge.put("from", graph.getEdgeSource(in));
			edge.put("to", graph.getEdgeTarget(in));
			result.accumulate("edges", edge);
		}
		for(Integer var : graph.vertexSet()) {
			JSONObject node = new JSONObject();
			node.put("id", var);
			node.put("label", var);
			result.accumulate("nodes", node);
		}
		return result;
	}
	
	private static void addVertex(DirectedGraph<Integer, Pair<Integer, SSAInstructionOrConstantWrapper>> graph, int vertex) {
		graph.addVertex(vertex);
		for (IObserver observer : observers)
			observer.onVertexAdded(vertex);
	}
	
	private static void addEdge(DirectedGraph<Integer, Pair<Integer, SSAInstructionOrConstantWrapper>> graph,
			int src, int tgt, SSAInstructionOrConstantWrapper lbl) {
		graph.addVertex(src);
		graph.addVertex(tgt);
		boolean hasEdge = !graph.getAllEdges(src, tgt).isEmpty();
		boolean addedEdge = graph.addEdge(src, tgt, Pair.make(edgecounter++, lbl)); 
		assert hasEdge || addedEdge;
		// if(! addedEdge)		
		//	System.out.println("Edge from "+src+" to "+tgt+" not added - "+" instruction "+lbl);
		for (IObserver observer : observers)
			observer.onEdgeAdded(src, tgt, format(lbl));
	}
	
	private static String format(SSAInstructionOrConstantWrapper w) {
		SSAInstruction lbl = w.getInst();
		if(lbl==null) return w.getConstant().toString();
		StringBuilder result = new StringBuilder();
		result.append("<");
		result.append(type(lbl));
		result.append(" , ");
		int[] uses = new int[lbl.getNumberOfUses()];
		for (int i=0; i<uses.length; ++i)
			uses[i] = lbl.getUse(i);
		result.append(Arrays.toString(uses));
		if (lbl.hasDef())
			result.append(String.format(" => %d", lbl.getDef()));
		result.append(" >");
		return result.toString();
	}

	private static Object type(SSAInstruction lbl) {
		if (lbl instanceof SSAReturnInstruction)
			return "return";
		else if (lbl instanceof SSAConditionalBranchInstruction)
			return "cond branch";
		else if (lbl instanceof SSAAbstractInvokeInstruction)
			return "invoke";
		else if (lbl instanceof SSAAbstractThrowInstruction)
			return "throw";
		else if (lbl instanceof SSAAbstractUnaryInstruction)
			return "unary op";
		else if (lbl instanceof SSAArrayLengthInstruction)
			return "array len";
		else if (lbl instanceof SSAArrayReferenceInstruction)
			return "array ref";
		else if (lbl instanceof SSABinaryOpInstruction)
			return "binary op";
		else if (lbl instanceof SSACheckCastInstruction)
			return "check cast";
		else if (lbl instanceof SSAComparisonInstruction)
			return "comparison";
		else if (lbl instanceof SSAConversionInstruction)
			return "conversion";
		else if (lbl instanceof SSAFieldAccessInstruction)
			return "field access";
		else if (lbl instanceof SSAGetCaughtExceptionInstruction)
			return "get caught ex";
		else if (lbl instanceof SSAGotoInstruction)
			return "goto";
		else if (lbl instanceof SSAInstanceofInstruction)
			return "instance of";
		else if (lbl instanceof SSAMonitorInstruction)
			return "monitor";
		else if (lbl instanceof SSANewInstruction)
			return "new";
		else if (lbl instanceof SSAPhiInstruction)
			return "phi";
		else if (lbl instanceof SSASwitchInstruction)
			return "switch";
		else
			throw new RuntimeException(lbl.toString());
	}
	
	public static DirectedGraph<Integer, Pair<Integer, SSAInstructionOrConstantWrapper>> doIntraprocAnalysis(IMethod mthd, IntSet srcs, String fileout) throws InvalidClassFileException {
		
		for (IObserver observer : observers)
			observer.onNewGraph(mthd.getName().toString()+mthd.getDescriptor().toString());
		
		JSONObject classesToTrackJSON;
		try {
			InputStream is = new FileInputStream("../results/"+fileout+".json");
            classesToTrackJSON = new JSONObject(IOUtils.toString(is));
		    is.close();
		} catch (Exception e1) {
		    classesToTrackJSON = new JSONObject();
		}
	    
		Set<Integer> conditionalOpcodes = new HashSet<Integer>();
		Set<Integer> returnOpcodes      = new HashSet<Integer>();

		conditionalOpcodes.add(153);	// 0x99 ifeq	
		conditionalOpcodes.add(154);	// 0x9A ifne	
		conditionalOpcodes.add(155);	// 0x9B iflt	
		conditionalOpcodes.add(156);	// 0x9C ifge	
		conditionalOpcodes.add(157);	// 0x9D ifgt	
		conditionalOpcodes.add(158);	// 0x9E ifle	
		conditionalOpcodes.add(159);	// 0x9F if_icmpeq	
		conditionalOpcodes.add(160);	// 0xA0 if_icmpne	
		conditionalOpcodes.add(161);	// 0xA1 if_icmplt	
		conditionalOpcodes.add(162);	// 0xA2 if_icmpge	
		conditionalOpcodes.add(163);	// 0xA3 if_icmpgt	
		conditionalOpcodes.add(164);	// 0xA4 if_icmple	
		conditionalOpcodes.add(165);	// 0xA5 if_acmpeq	
		conditionalOpcodes.add(166);	// 0xA6 if_acmpne			
		conditionalOpcodes.add(198);	// 0xC6 ifnull	
		conditionalOpcodes.add(199);	// 0xC7 ifnonnull
		
		// we have to count goto and jsr as conditional since, in the instrumenter side, ASM count them as jump instructions
		// http://asm.ow2.org/asm50/javadoc/user/org/objectweb/asm/tree/MethodNode.html#visitJumpInsn%28int,%20org.objectweb.asm.Label%29
		conditionalOpcodes.add(167);	// 0xA7 goto	
		conditionalOpcodes.add(168);	// 0xA8 jsr	
				
		// returnOpcodes.add(177);
		
		@SuppressWarnings("unchecked")
		DirectedGraph<Integer, Pair<Integer, SSAInstructionOrConstantWrapper>> result = new DirectedPseudograph<Integer, Pair<Integer, SSAInstructionOrConstantWrapper>>((Class<? extends Pair<Integer, SSAInstructionOrConstantWrapper>>) Pair.class);

		IR ir = new DexIRFactory().makeIR(mthd, Everywhere.EVERYWHERE, SSAOptions.defaultOptions());
		addVertex(result, SOURCE);
		for (IntIterator iit = srcs.intIterator(); iit.hasNext(); ) {
			int v = iit.next();
			addVertex(result, v);
			SSAInstruction ass = new DefUse(ir).getDef(v);
			if(ass != null && (
					ass instanceof SSAAbstractInvokeInstruction ||
					ass instanceof SSAGetInstruction ||
					ass instanceof SSANewInstruction
				))
				addEdge(result, SOURCE, v, new SSAInstructionOrConstantWrapper(ass));
			else {
				try {
					SymbolTable sb = ir.getSymbolTable();
					//String value = sb.getStringValue(v);
					Object value = sb.getConstantValue(v);
					if(value!=null) 
						addEdge(result, SOURCE, v, new SSAInstructionOrConstantWrapper(value));
					else System.out.println("Source "+v+" not added");
				}
				catch(IllegalArgumentException e) {
					System.out.println("Source "+v+" not added (IllegalArgumentException)");
				}
			}
		}
		addVertex(result, STOP);
		
		DefUse du = new DefUse(ir);
		ControlDependenceGraph<SSAInstruction, ISSABasicBlock> cdg = new ControlDependenceGraph<SSAInstruction, ISSABasicBlock>(ir.getControlFlowGraph());
		MutableIntSet worklist = MutableSparseIntSet.make(srcs);
		Set <Integer>bcIndexes = new HashSet<Integer>();
		while (!worklist.isEmpty()) {
			IntIterator it = worklist.intIterator();
			worklist = MutableSparseIntSet.makeEmpty();
			while (it.hasNext()) {
				int nxt = it.next();
				for (Iterator<SSAInstruction> uses = du.getUses(nxt); uses.hasNext(); ) {
					SSAInstruction instruction = uses.next();
					if (instruction instanceof SSAConditionalBranchInstruction) {
						IBytecodeMethod method = (IBytecodeMethod)ir.getMethod();
						int bytecodeIndex = method.getBytecodeIndex(instruction.iindex);
						bcIndexes.add(bytecodeIndex);
						addEdge(result, nxt, STOP, new SSAInstructionOrConstantWrapper(instruction,bytecodeIndex));
						if(! AnalysisEngine.conditions_as_sinks) {
							if(! result.containsVertex(instruction.getUse(0)))
								addVertex(result, instruction.getUse(0));
							
							
							Set<ISSABasicBlock> bb_worklist = addAll(cdg.getSuccNodes(ir.getBasicBlockForInstruction(instruction)), new HashSet<ISSABasicBlock>());
							Set<ISSABasicBlock> previous = null;
							while(previous==null || ! bb_worklist.equals(previous)) {
								previous = new HashSet<ISSABasicBlock>(bb_worklist);
								for(ISSABasicBlock block : previous)
									addAll(cdg.getSuccNodes(block), bb_worklist);
							}
							//Iterator<ISSABasicBlock> c = cdg.getSuccNodes(ir.getBasicBlockForInstruction(instruction));
							Iterator<ISSABasicBlock> c = bb_worklist.iterator();
							addEdge(result, nxt, STOP, new SSAInstructionOrConstantWrapper(instruction,bytecodeIndex));
							while(c.hasNext()) {
								ISSABasicBlock block = c.next();
								for(SSAInstruction inst : block) {
									if(inst.hasDef()) {
										int def = inst.getDef();
										if (!result.containsVertex(def)) {
											worklist.add(def);
											addVertex(result, def);
										}
										addEdge(result, nxt, def, new SSAInstructionOrConstantWrapper(inst));
									} else  if (inst instanceof SSAReturnInstruction) {
										IBytecodeMethod method1 = (IBytecodeMethod)ir.getMethod();
										int bytecodeIndex1 = method1.getBytecodeIndex(inst.iindex);
										bcIndexes.add(bytecodeIndex1);
										addEdge(result, nxt, STOP, new SSAInstructionOrConstantWrapper(inst,bytecodeIndex));
									}
								}
							}
						}
					} else if (instruction instanceof SSAReturnInstruction) {
						IBytecodeMethod method = (IBytecodeMethod)ir.getMethod();
						int bytecodeIndex = method.getBytecodeIndex(instruction.iindex);
						bcIndexes.add(bytecodeIndex);
						addEdge(result, nxt, STOP, new SSAInstructionOrConstantWrapper(instruction,bytecodeIndex));
					} else if (instruction instanceof SSAAbstractInvokeInstruction && 
							((SSAAbstractInvokeInstruction) instruction).getDeclaredTarget().getName().toString().equals("<init>")) {
						int use0 = instruction.getUse(0);
						if (!result.containsVertex(use0)) {
							worklist.add(use0);
							addVertex(result, use0);
						}
						addEdge(result, nxt, use0, new SSAInstructionOrConstantWrapper(instruction));
					} else if (instruction.hasDef()) {
						int def = instruction.getDef();
						if (!result.containsVertex(def)) {
							worklist.add(def);
							addVertex(result, def);
						}
						addEdge(result, nxt, def, new SSAInstructionOrConstantWrapper(instruction));
					}
				}
				
			}
		}

		String klass = mthd.getDeclaringClass().getName().toString().substring(1)+".class";
		// System.out.print(klass);
		// System.out.print(" "+mthd.getName().toString()+mthd.getDescriptor().toString());
		// System.out.println(bcIndexes);
		if(! (ir.getMethod() instanceof FakeRootMethod)) {
			IBytecodeMethod method = (IBytecodeMethod)ir.getMethod();
			InstructionArrayWrapper instructions = getInstructions(method);
			int count_if = 0;
			@SuppressWarnings("unused")
			int count_return = 0;
	
			for(int i=0; i< instructions.length(); i++) {
				int bytecodeIndex = method.getBytecodeIndex(i);
				int opcode = instructions.getOpcode(i);
				if (conditionalOpcodes.contains(opcode)){
					count_if++;
					if(bcIndexes.contains(bytecodeIndex)){
						classesToTrackJSON = addInClassesToTrackJSON(classesToTrackJSON, klass, mthd.getName().toString()+mthd.getDescriptor().toString(), "conditional", count_if);
						// System.out.println(classesToTrackJSON);					
						// System.out.println("->"+bytecodeIndex+" "+instructions.toString(i) + "("+opcode+")");
					}
				} else if (returnOpcodes.contains(opcode)) {
					count_return++; //TODO as conditional
					// System.out.println("->"+bytecodeIndex+" "+instructions.toString(i) + "("+opcode+")");
				} else {
					// System.out.println(bytecodeIndex+" "+instructions.toString(i) + "("+opcode+")");
				}
			}
		}
		//for(SSAInstruction inst : node.getIR().getInstructions()) {
		//	int bytecodeIndex = method.getBytecodeIndex(inst.iindex);
		//	if(inst instanceof SSAReturnInstruction) {
		//		count_return++;
		//		if(bcIndexes.contains(new Integer(bytecodeIndex))) {
		//			//add count_return;
		//		}
		//	}
		//	else if(inst instanceof SSAConditionalBranchInstruction) {
		//		count_if++;
		//		if(bcIndexes.contains(new Integer(bytecodeIndex))) {
		//			//add count_if;
		//		}
		//	}
		//}
		
		try {
		    FileWriter file = new FileWriter("../results/"+fileout+".json");
			file.write(classesToTrackJSON.toString());
	        file.flush();
	        file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	private static InstructionArrayWrapper getInstructions(
			IBytecodeMethod method) throws InvalidClassFileException {
		if (method instanceof DexIMethod)
			return new InstructionArrayWrapper(((DexIMethod) method).getDexInstructions());
		else
			return new InstructionArrayWrapper(method.getInstructions());
	}

	private static <T> Set<T> addAll(Iterator<T> it, Set<T> set) {
		while(it.hasNext())
			set.add(it.next());
		return set;
	}
	
	private static JSONObject addInClassesToTrackJSON( JSONObject classesToTrackJSON, String klass, String method, String category, int occurence){		
		JSONObject _klass   = new JSONObject();
		JSONObject _method  = new JSONObject();
		JSONArray _category = new JSONArray();
		
		try {
		  if (classesToTrackJSON.has(klass)){ _klass = classesToTrackJSON.getJSONObject(klass);}
		  if ( _klass.has(method)){ _method = _klass.getJSONObject(method);}
		  if (_method.has(category)){ _category = _method.getJSONArray(category);}
		  
		  Boolean isThere = false;
		  for (int i=0; i<_category.length(); ++i) {
          	if (occurence == (Integer)_category.get(i)){
          		isThere = true; break;
          	}
          }
		    if (!isThere){ _category.put(occurence);
			_method.put(category, _category);
			_klass.put(method,_method);
			classesToTrackJSON.put(klass,_klass);
		    }
		
		} catch (JSONException e) {
			// This should never happen
			e.printStackTrace();
		}
		return classesToTrackJSON;
	}
}
