#!/bin/bash

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat.mac
else
    echo "on non-MAC"
    . myconfig.dat
fi

die () { echo "This step went wrong"; exit 1; }

if [ "$verbose" = 1 ]; then
    exec 4>&2 3>&1
else
    exec 4>/dev/null 3>/dev/null
fi

if [ -z $1 ]
then
  echo "give a log to eat"
  exit 1
fi

if [ -z $2 ]
then
  echo "give me an idx file too!"
  exit 1
fi

if [ -z $3 ]
then
  echo "give me an wl file too!"
  exit 1
fi

if [ -z $4 ]
then
  counter=$(tail -n1 $2 | cut -f 1 -d "|")
else
  counter=$4
fi
FILE=$2
FILEtwo=$1
bn=$(basename ${FILE%.*})
bntwo=$(basename ${FILEtwo%.*})
case=${bntwo#*-}

if [ -f $1 ]
then

./mygrep.py $1 | sort | uniq | while read p; do
  fp=$(grep "$(echo $p | sed -e "s/\[/\\\[/g")$" $2 | cut -f 1 -d '|') 

  if [ -z $fp ]
  then
      echo -e "\tthe flipping point $(grep "$(echo $p | sed -e "s/\[/\\\[/g")$" results/$bn.pending | cut -f 1 -d '|') was already counted"
  else
     counter=$(($((10#$counter))+1))
     counterS=$(printf %04d $counter)
     if [ '0000' == "$case" ]
     then
         echo -e "\troot flipping point found: $fp"
         toAdd=$fp
     else
         toAdd=$counterS
         echo -e "\tnew flipping point found: $fp. To be merged with $case in $toAdd"
         ./merge-jsons.py results/$bn-$fp.json results/$bn-$case.json > results/$bn-$toAdd.json
     fi
     echo -e "\tadding $toAdd to the worklist"
     echo $toAdd >> $3 
     
if [ "$(uname -s)" == "Darwin" ]; then
     sed -i "" "/^$fp|/d" $2
else
     sed -i "/^$fp|/d" $2
fi
  fi
done 

exit 0

else
  echo "I cannot find $1"
  exit 1
fi
