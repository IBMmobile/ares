#!/usr/bin/python

import json
import sys
import itertools
import os


infiles = sys.argv[1:]
ret = {}

def merge(obj1,obj2):
  if obj1 is None: return obj2
  if obj2 is None: return obj1

  if type(obj1)==dict:
    ret={}
    for key in obj1.keys()+obj2.keys():
        ret[key]=merge(obj1.setdefault(key,None),obj2.setdefault(key,None))
    return ret
  elif type(obj1)==list:
    return list(set(obj1+obj2))

for infile in infiles:
  with open(infile) as data_file:    
    classes = json.load(data_file)
    ret=merge(ret,classes)
print json.JSONEncoder().encode(ret)
