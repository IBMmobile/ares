#!/bin/bash

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat
else
    echo "on non-MAC"
    . myconfig.dat.mac
fi

die () { echo "This step went wrong"; exit 1; }

if [ "$verbose" = 1 ]; then
    exec 4>&2 3>&1
else
    exec 4>/dev/null 3>/dev/null
fi

if [ -z $1 ]
then
  echo "give the apk to eat"
  exit 1
fi

if [ -z $2 ]
then
   emulator="emulator-5554"
else
   emulator="$2"
fi

APK=$1
APKbn=$(basename ${APK%.*})
APP=$($aaptpwd d badging $APK | grep package | cut -f 2 -d "'")

echo " = installing $APK in $emulator"
$adbcmd -s $emulator install $APK || die

echo " = clean the $emulator log"
$adbcmd -s $emulator logcat -c

#echo " = granting SEND_SMS to $APP"
#$adbcmd -s $emulator shell pm grant $APP android.permission.SEND_SMS 

echo " = launch the app"
$adbcmd -s $emulator shell monkey -p $APP -c android.intent.category.LAUNCHER -s 0 1
$adbcmd -s $emulator shell monkey -p $APP -c android.intent.action.BOOT_COMPLETED -s 0 1

#echo " = tap around"
#$adbcmd -s $emulator shell monkey -p $APP --throttle 20 -s 0 100  >&3 2>&4 || die
#sleep 7

echo " = trigger events"
for INTENT in $($aaptpwd l -a $APK | grep "            A: android:name(0x01010003)=\"android." | cut -f '2' -d "\"" | sort | uniq)
do
if [ "android.intent.category.LAUNCHER" != $INTENT -a "android.provider.Telephony.SMS_DELIVER" != $INTENT ]
then
echo -e "\t= $INTENT"
$adbcmd -s $emulator shell am broadcast -a $INTENT -p $APP &
$adbcmd -s $emulator shell monkey -p $APP -c $INTENT -s 0 1
fi
done
sleep 5

$adbcmd -s $emulator logcat -d -s  ActivityManager:I  | grep "Start proc" | cut -f 4 -d ':' | cut -f 4 -d ' ' > $APKbn.pid

echo " = stopping $APP"
$adbcmd -s $emulator shell am force-stop $APP

echo " = collecting the log: results/$APKbn.log"
echo "PID(s): $(cat $APKbn.pid)"
$adbcmd -s $emulator logcat -d -s System:I IBM:* evadroid:* | grep -f $APKbn.pid > results/$APKbn.log

$adbcmd -s $emulator logcat -d > results/$APKbn-unfiltered.log

echo " = uninstalling $APP"
$adbcmd -s $emulator uninstall $APP

exit 0
