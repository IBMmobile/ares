#!/usr/bin/python

import json
import sys
import itertools
import os


def save(filename,klass,method,category,o):
  print filename,klass,method,category,o
  with open(filename, 'w') as outfile:
    json.dump({klass:{method:{category:o}}}, outfile)

infile = sys.argv[1]
basename = os.path.splitext(infile)[0]
i=0

with open(infile) as data_file:    
  classes = json.load(data_file)
  for klass, methods in classes.iteritems():
    for method,categories in methods.iteritems():
      conditionals = categories["conditional"] 
      for size in range(1,len(conditionals)+1): 
        for c in itertools.combinations(conditionals,size):
           i+=1;
           save("%s-%.4i.json" % (basename,i), klass, method, "conditional", list(c))
