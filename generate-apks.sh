#!/bin/sh

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat.mac
else
    echo "on non-MAC"
    . myconfig.dat
fi

die () { echo "This step went wrong"; exit 1; }

if [ "$verbose" = 1 ]; then
    exec 4>&2 3>&1
else
    exec 4>/dev/null 3>/dev/null
fi

if [ -z $1 ]
then
  echo "give the apk to eat"
  exit 1
fi

FILE=$1
bn=$(basename ${FILE%.*})

if [ -f $1 ]
then

[ -e /usr/local/bin/d2j-dex2jar ] || brew install dex2jar
[ -e /usr/local/bin/apktool ] || brew install apktool

echo " = unpacking $FILE"
./unpack.sh $FILE >&3 2>&4 || die
echo "\tunpacked in $bn/"

echo " = slicing analysis"
if [ -e results/$bn.json  ]
then
  echo "\tremoving previous result results/$bn.json and dependencyBuilder/graph/dat/$bn-*";
  rm results/$bn.json;
  rm dependencyBuilder/graph/dat/$bn-*
fi
cd dependencyBuilder/
$javacmd  -Dandroidjar=$androidjar -Dfile.encoding=UTF-8 -classpath ./bin:./lib/jgrapht-core-0.9.1.jar:./lib/jgraph-5.13.0.0.jar:./lib/org.json.jar:./lib:./lib/walautil.jar evasion.core.AnalysisEngine ../$bn/$bn.jar >&3 2>&4 || die
cd ..
echo "\tthe graph can be found in file://$(pwd)/dependencyBuilder/graph/index.html"

echo " = creating combinations $FILE"
if [ -e results/$bn-0001.json  ]
then
  echo "\tremoving previous result results/$bn-*.json";
  rm results/$bn-*.json;
fi
./combinations.py results/$bn.json >&3 2>&4 || die
echo "\t$(ls results/$bn-*.json | wc -l | tr -d '[[:space:]]') combinations generated"

echo " = instrumenting and repacking each case"
for json in $(ls results/$bn-*.json)
do
  cd conditionalFlipper/
  apk=$(basename ${json%.*})
  echo "\tinstrumenting $apk"
  $javacmd -Dfile.encoding=UTF-8 -classpath ./bin:./lib/asm-all-5.0.3.jar:./lib/commons-io-2.4.jar:./lib/org.json.jar instrumenter.asm.Instrumenter ../$bn/$bn.jar.dir ../$bn/$apk-new ../$json >&3 2>&4 || die
  cd ..
  echo "\trepacking $apk-new"
  ./repack.sh $bn $apk-new >&3 2>&4 || die
  echo -e "\tmoving result to results/$apk-new.apk"
  mv $apk-new.apk results/
done

exit 0

echo " - installing: $1-new.apk"
$adbcmd -s emulator-5554 uninstall de.ecspride
$adbcmd -s emulator-5554 install $newNumber.apk

else
  echo "I cannot find $1"
  exit 1
fi
