#!/bin/bash

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat.mac
else
    echo "on non-MAC"
    . myconfig.dat
fi

die () { echo "This step went wrong"; exit 1; }

if [ "$verbose" = 1 ]; then
    exec 4>&2 3>&1
else
    exec 4>/dev/null 3>/dev/null
fi

if [ ! -z $1 -o -d $1/$1.jar.dir ]
then 
  FILE=$1
  bn=$(basename ${FILE%.*})
else
  echo "I cannont find the directory $1/$1.jar.dir"
  exit 1
fi

if [ "$(uname -s)" == "Darwin" ]; then
  [ -e /usr/local/bin/d2j-dex2jar ] || brew install dex2jar
  [ -e /usr/local/bin/apktool ] || brew install apktool
fi

cd conditionalFlipper/
echo " = searching for sleeping calls in $bn/$bn.jar.dir"
./DoesCallSleep.sh $bn

echo " = instrumenting $bn/$bn.jar.dir into $bn/$bn-sleep"
$javacmd -Dfile.encoding=UTF-8 -classpath ./bin:./lib/asm-all-5.0.3.jar:./lib/commons-io-2.4.jar:./lib/org.json.jar instrumenter.asm.InstrumenterSleep ../$1/$1.jar.dir/ ../$1/$1-sleep/ ../results/$1.sleep > ../results/$1-out.sleep || die
cd ..

echo " = repacking $bn-sleep.apk"
./repack.sh $bn $bn-sleep >&3 2>&4 || die
echo -e "\tmoving result to results/$bn-sleep.apk"
mv $bn-sleep.apk results/
