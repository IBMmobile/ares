#!/bin/bash

if [ "$(uname -s)" == "Darwin" ]; then
    echo "on MAC"
    . myconfig.dat.mac
else
    echo "on non-MAC"
    . myconfig.dat
fi

if [ -z $1 ]
then
  echo "give the apk to eat"
  exit 1
fi

FILE=$1
bn=$(basename ${FILE%.*})

if [ -e $1 ]
then

if [ "$(uname -s)" == "Darwin" ]; then
[ -e /usr/local/bin/d2j-dex2jar ] || brew install dex2jar
[ -e /usr/local/bin/apktool ] || brew install apktool
fi

echo " - unpack the $bn.apk"
if [ "$(uname -s)" == "Darwin" ]; then
 apktool d $bn.apk -s -f -o $bn
else
 ./apktool d $bn.apk -s -f -o $bn
fi

echo " - convert classes.dex to jar format"
cd $bn
if [ "$(uname -s)" == "Darwin" ]; then
   d2j-dex2jar classes.dex -o $bn.jar
else
   sh ../dex2jar-2.0/d2j-dex2jar.sh classes.dex -o $bn.jar
fi

echo " - unpack jar"

mkdir $bn.jar.dir
cd $bn.jar.dir
jar -xf ../$bn.jar
cd ..

else
  echo "I cannot find $1"
  exit 1
fi
