package instrumenter.asm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.json.JSONObject;
import org.json.JSONException;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

public class InstrumenterSleep {

	static final String CLASS_FILE_REGEX = "[^\\s]+\\.class$";
	
	public static void main(String[] args) throws IOException, JSONException {
		File infolder  = new File(args[0]);
		File outfolder = new File(args[1]);

		//classes with sleep calls
		List<String> classesWithSleep;
		try{
			classesWithSleep = Files.readAllLines(Paths.get(args[2]), StandardCharsets.UTF_8);
		}catch (ArrayIndexOutOfBoundsException e){
			classesWithSleep = new ArrayList<String>();
		}
		
		List<String[]> sleepingFunctions = new ArrayList<String[]>();
		try {
		    for (String line : Files.readAllLines(Paths.get("sleepingFunctions.list"), StandardCharsets.UTF_8)) {
		    	String[] colon = line.split(":");
		    	String[] dot = colon[0].split("\\.");
		    	sleepingFunctions.add(new String[]{dot[0],dot[1],colon[1]});
		    }
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
		
		Collection<File> classfiles = FileUtils.listFiles(infolder, new RegexFileFilter(CLASS_FILE_REGEX), DirectoryFileFilter.DIRECTORY);

		L: for (File classfile : classfiles) {
			String classPath = classfile.getPath().replace(infolder.getPath()+"/", "");
			if (!classesWithSleep.contains(classPath)) {
				File tgtfile = new File(classfile.getPath().replace(infolder.getPath(), outfolder.getPath()));
				FileUtils.copyFile(classfile, tgtfile);
				continue L;
			}

	        byte[] inbytes = FileUtils.readFileToByteArray(classfile);
	        ClassReader cr = new ClassReader(inbytes);
	        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

			ClassVisitor cv = new ClassAdapterSleep(cw,sleepingFunctions);

			byte[] outbytes;
			try {
				cr.accept(cv, 0);
				outbytes = cw.toByteArray();
			} catch (RuntimeException e) {
				outbytes = inbytes;
				System.err.println(String.format("</skipping: %s>", classfile));
			}
			File instrumentedClassFile = new File(classfile.getPath().replace(infolder.getPath(), outfolder.getPath()));
			FileUtils.writeByteArrayToFile(instrumentedClassFile, outbytes);
		}
	}
}
