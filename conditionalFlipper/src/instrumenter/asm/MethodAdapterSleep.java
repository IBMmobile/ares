package instrumenter.asm;

import java.util.List;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class MethodAdapterSleep extends MethodVisitor{
    private List<String[]> sleepingFunctions;

	public MethodAdapterSleep(final MethodVisitor mv, List<String[]> sleepingFunctions) {
        super(Opcodes.ASM5, mv);
        this.sleepingFunctions = sleepingFunctions;
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
		for(String[] s : this.sleepingFunctions) {
			if (s[0].equals(owner) && s[1].equals(name) && s[2].equals(desc)) {
				System.out.println(" Sleep function found!: "+s[0]+"."+s[1]+":"+s[2]);
				// mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "logTopLong1", "(J)J");
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "topLongToZero", "(J)J");
				// mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "logTopLong1", "(J)J");
			}
		}
		mv.visitMethodInsn(opcode, owner, name, desc, itf);
    }
}
