package instrumenter.asm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.json.JSONObject;
import org.json.JSONException;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

public class Instrumenter {

	static final String CLASS_FILE_REGEX = "[^\\s]+\\.class$";
	
	public static void main(String[] args) throws IOException, JSONException {
		File infolder  = new File(args[0]);
		File outfolder = new File(args[1]);		
		InputStream isToFlip = new FileInputStream(args[2]);
        JSONObject classesToFlip = new JSONObject(IOUtils.toString(isToFlip));
        JSONObject classesToLog;

		try{
			InputStream isToLog  = new FileInputStream(args[3]);
	        classesToLog  = new JSONObject(IOUtils.toString(isToLog));
		}catch (ArrayIndexOutOfBoundsException e){
			classesToLog  = new JSONObject();
		}
        
		Collection<File> classfiles = FileUtils.listFiles(infolder, new RegexFileFilter(CLASS_FILE_REGEX), DirectoryFileFilter.DIRECTORY);

		L: for (File classfile : classfiles) {
			String classPath = classfile.getPath().replace(infolder.getPath()+"/", "");
			if (!classesToFlip.has(classPath) && !classesToLog.has(classPath)) {
				File tgtfile = new File(classfile.getPath().replace(infolder.getPath(), outfolder.getPath()));
				FileUtils.copyFile(classfile, tgtfile);
				continue L;
			}

			try {				
				ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
				JSONObject toFlip;
				try { toFlip = classesToFlip.getJSONObject(classPath); }
			    catch (JSONException e) { toFlip = new JSONObject();}
				
				JSONObject toLog;
				try { toLog = classesToLog.getJSONObject(classPath); }
			    catch (JSONException e) { toLog = new JSONObject(); }
				
				ClassVisitor cv = new ClassAdapter(cw, toFlip, toLog);

				byte[] inbytes = FileUtils.readFileToByteArray(classfile);
				ClassReader cr = new ClassReader(inbytes);
				cr.accept(cv, 0);
				
				byte[] outbytes;
				try {
					outbytes = cw.toByteArray();
				} catch (RuntimeException e) {
					System.err.println(String.format("<skipping: %s>", classfile));
					e.printStackTrace();
					outbytes = inbytes;
					System.err.println(String.format("</skipping: %s>", classfile));
				}
				File instrumentedClassFile = new File(classfile.getPath().replace(infolder.getPath(), outfolder.getPath()));
				FileUtils.writeByteArrayToFile(instrumentedClassFile, outbytes);				
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}
}
