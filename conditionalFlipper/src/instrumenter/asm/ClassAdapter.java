package instrumenter.asm;

import org.json.JSONException;
import org.json.JSONObject;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class ClassAdapter extends ClassVisitor {

	private String klass;
	private boolean isInterface;
	private JSONObject methodsToFlip;
	private JSONObject methodsToLog;
	
	public ClassAdapter(ClassVisitor cv, JSONObject mapToFlip, JSONObject mapToLog) {
		super(Opcodes.ASM5, cv);
		this.methodsToFlip = mapToFlip;
		this.methodsToLog  = mapToLog;
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		cv.visit(version, access, name, signature, superName, interfaces);
		klass = name;
		isInterface = (access & Opcodes.ACC_INTERFACE) != 0;
	}
	
	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions){
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
		if (!isInterface && mv != null && ( methodsToFlip.has(name+desc) || methodsToLog.has(name+desc))){
			JSONObject toFlip;
			try { toFlip = methodsToFlip.getJSONObject(name+desc); }
		    catch (JSONException e) { toFlip = new JSONObject();}
			
			JSONObject toLog;
			try { toLog = methodsToLog.getJSONObject(name+desc); }
		    catch (JSONException e) { toLog = new JSONObject();}
		    
			mv = new MethodAdapter(mv, klass, name, desc, (access & Opcodes.ACC_STATIC) != 0, toFlip, toLog);
		}
		return mv;
	}
}
