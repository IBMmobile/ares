package instrumenter.asm;

import java.util.List;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class ClassAdapterSleep extends ClassVisitor {
	
	private List<String[]> sleepingFunctions;

	public ClassAdapterSleep(ClassVisitor cv, List<String[]>sleepingFunctions) {
		super(Opcodes.ASM5, cv);
		this.sleepingFunctions = sleepingFunctions;
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions){
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
	    return new MethodAdapterSleep(mv,this.sleepingFunctions);
	}
}