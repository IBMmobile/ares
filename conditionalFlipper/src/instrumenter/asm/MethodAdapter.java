/**
 * 
 */
package instrumenter.asm;

import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.CodeSizeEvaluator;

public class MethodAdapter extends MethodVisitor {

	private final String klass;
	private final String mname;
	private final String mdesc;
	private final boolean isStatic;
	private final int size;
	private int counter=0;
	private Set<Integer> occurrencesCond2F = new HashSet();
	private Set<Integer> occurrencesRet2F = new HashSet();
	private Set<Integer> occurrencesCond2L = new HashSet();
	private Set<Integer> occurrencesRet2L = new HashSet();
	
	public MethodAdapter(MethodVisitor mv, String klass, String mname, String mdesc, boolean isStatic, JSONObject kindAndOccurences2F, JSONObject kindAndOccurences2L) {
		super(Opcodes.ASM5, mv);
		this.klass = klass;
		this.mname = mname;
		this.mdesc = mdesc;
		this.isStatic = isStatic;
		this.size = new CodeSizeEvaluator(mv).getMaxSize();
		JSONArray a;
		
		try {
			a = kindAndOccurences2F.getJSONArray("conditional");
			for(int i = 0; i < a.length(); i++){
				occurrencesCond2F.add(a.getInt(i));
			}
		} catch (JSONException e) { }
		try {
			a = kindAndOccurences2F.getJSONArray("return");
			for(int i = 0; i < a.length(); i++){
				this.occurrencesRet2F.add(a.getInt(i));  
			}
		} catch (JSONException e) { }

		try {
			a = kindAndOccurences2L.getJSONArray("conditional");
			for(int i = 0; i < a.length(); i++){
				occurrencesCond2L.add(a.getInt(i));
			}
		} catch (JSONException e) { }
		try {
			a = kindAndOccurences2L.getJSONArray("return");
			for(int i = 0; i < a.length(); i++){
				this.occurrencesRet2L.add(a.getInt(i));  
			}
		} catch (JSONException e) { }
	
	}
	
	@Override
	public void visitJumpInsn(int opcode, Label label) {
		counter++;

		if(occurrencesCond2L.contains(counter)){
			mv.visitLdcInsn(this.klass+" "+this.mname+this.mdesc+":"+this.counter);
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "checkpoint", "(Ljava/lang/String;)V");
			System.out.println("instrumenting2Log: "+opcode+" - "+counter);
		}
		
		if (opcode == Opcodes.IFEQ // 153	0x99        
		 || opcode == Opcodes.IFNE // 154	0x9A        
         || opcode == Opcodes.IFLT // 155	0x9B     
         || opcode == Opcodes.IFGE // 156	0x9C     
         || opcode == Opcodes.IFGT // 157	0x9D     
         || opcode == Opcodes.IFLE // 158	0x9E     
		    ){
			if(occurrencesCond2F.contains(counter)){
				mv.visitIntInsn(Opcodes.SIPUSH, opcode); //push the opcode in a short
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "flip1", "(IS)I");
				System.out.println("instrumenting: "+opcode+" - "+counter);
			}
		} else if (opcode == Opcodes.IFNULL    // 198	0xC6
		        || opcode == Opcodes.IFNONNULL // 199	0xC7
		    ){	
		    if(occurrencesCond2F.contains(counter)){
		    	// mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "logTopRef", "(Ljava/lang/Object;)Ljava/lang/Object;");
		    	mv.visitIntInsn(Opcodes.SIPUSH, opcode); //push the opcode in a short
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "flipRef1", "(Ljava/lang/Object;S)Ljava/lang/Object;");
				System.out.println("instrumenting: "+opcode+" - "+counter);    	
		    }
		} else if (opcode == Opcodes.IF_ACMPEQ // 165	0xA5
				|| opcode == Opcodes.IF_ACMPNE // 166	0xA6
		    ){
		    if(occurrencesCond2F.contains(counter)){
				mv.visitInsn(Opcodes.DUP);
				mv.visitVarInsn(Opcodes.ASTORE, 255);				
				mv.visitIntInsn(Opcodes.SIPUSH, opcode); //push the opcode in a short
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "flipRef2", "(Ljava/lang/Object;Ljava/lang/Object;S)Ljava/lang/Object;");
				mv.visitVarInsn(Opcodes.ALOAD, 255);
				
				System.out.println("instrumenting: "+opcode+" - "+counter);    	
		    }
		} else if ( opcode == Opcodes.IF_ICMPEQ // 159	0x9F
			     || opcode == Opcodes.IF_ICMPNE // 160	0xA0
			     || opcode == Opcodes.IF_ICMPLT // 161	0xA1
			     || opcode == Opcodes.IF_ICMPGE // 162	0xA2
			     || opcode == Opcodes.IF_ICMPGT // 163	0xA3
			     || opcode == Opcodes.IF_ICMPLE // 164	0xA4
			){	
			if(occurrencesCond2F.contains(counter)){
				mv.visitInsn(Opcodes.DUP);
				mv.visitVarInsn(Opcodes.ISTORE, 255);				
				mv.visitIntInsn(Opcodes.SIPUSH, opcode); //push the opcode in a short
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/ibm/EvasionRuntime", "flip2", "(IIS)I");
				mv.visitVarInsn(Opcodes.ILOAD, 255);
				
				System.out.println("instrumenting: "+opcode+" - "+counter);
			}			
		}
		System.out.println("visited "+counter+ " times: "+this+" "+this.klass+" "+this.mname+this.mdesc+" opcode: "+opcode+" label: "+ label);
		mv.visitJumpInsn(opcode,label);
	}
}
