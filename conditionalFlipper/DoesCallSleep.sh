#!/bin/sh

cd ../$1/$1.jar.dir/
> ../../results/$1.sleep

for i in $(find * -type f); do
   SHA=$(shasum $i | cut -f 1 -d ' ')
   grep -q $SHA ../../conditionalFlipper/whitelist.txt
   if [ $? -ne 0 ]
   then
      if javap -c $i | grep -o -f ../../conditionalFlipper/sleepingFunctions.list;
      then
         echo "adding $i in results/$1.sleep"
         echo $i >> ../../results/$1.sleep
      else
         echo $SHA >> ../../conditionalFlipper/whitelist.txt
      fi
   fi
done
