#!/usr/bin/python
import sys
#while read p; do echo "BB $p AA"; done < <(grep "I IBM     : CheckPoint: " results/qemuFingerprinting-000.log | awk '{$1=$2=$3=$4=$5=$6=$7=$8="";sub(" *",""); print}' )
# 09-27 14:17:57.502 10673 10673 I IBM     : CheckPoint: com/ibm/qemuFingerprinting/MainActivity onCreate(Landroid/os/Bundle;)V:1

import re

#regex = '([\d]+) - - \[(.*?)\] "(.*?)" (\d+) - "(.*?)" "(.*?)"'
regex = '([\d -:]+)(I IBM     : CheckPoint: )(.*)'

for rawline in open(sys.argv[1]):
  line = rawline.strip('\n').strip('\r')
  if "I IBM     : CheckPoint: " in line:
       print re.match(regex, line).groups()[2]
