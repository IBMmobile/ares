package com.ibm;

import android.util.Log;

public class EvasionRuntime {
	public static int logTopInt1(int i) {
	       Log.d("IBM","on Top: " + i);
	       return i;
		}
	
	public static long logTopLong1(long i) {
	       Log.d("IBM","on Top: " + i);
	       return i;
		}

	public static Object logTopRef(Object i) {
	       Log.d("IBM","on Top: " + i);
	       return i;
		}
	
	public static void checkpoint(String text) {
	       Log.i("IBM","CheckPoint: " + text);
		}
	
	public static void isItLoaded(String text) {
	       Log.i("IBM","is " + text+ " loaded?:");
		}
	
	public static Object flipRef1(Object i, short opcode) {
	       Log.d("IBM","instrumenting Ref opcode: " +opcode + " - arg1 == null: " + (i == null));
	       if (opcode == 198){ return i == null ? new Object() : null; } // 198 IFNULL    0xC6
	       if (opcode == 199){ return i != null ? null : new Object(); } // 199 IFNONNULL 0xC7
	       return i;
		}

	public static Object flipRef2(Object i2, Object i1, short opcode) {
	    // returns a new value for i2   
		   Log.d("IBM","instrumenting Ref opcode: " +opcode + " - [ " + i1+ " , " + i2+" ]");
	       if (opcode == 165){ return i1 == i2 ? i1 : i1; } // IF_ACMPEQ 165	0xA5
	       if (opcode == 166){ return i1 != i2 ? i1 : i1; } // IF_ACMPNE 166	0xA6
	       return i2;
		}
	
	public static int flip1(int i, short opcode) {
       Log.d("IBM","instrumenting opcode: " +opcode + " - arg1:" + i);
       if (opcode == 153){ return i == 0 ?  1 :  0; } // 153 IFEQ 0x99 
       if (opcode == 154){ return i != 0 ?  0 :  1; } // 154 IFNE 0x9A 
       if (opcode == 155){ return i <  0 ?  1 : -1; } // 155 IFLT 0x9B 
       if (opcode == 156){ return i >= 0 ? -1 :  1; } // 156 IFGE 0x9C 
       if (opcode == 157){ return i >  0 ? -1 :  1; } // 157 IFGT 0x9D 
       if (opcode == 158){ return i <= 0 ?  1 : -1; } // 158 IFLE 0x9E 
       return i;
	}

	public static int flip2(int i2, int i1, short opcode) {
       // returns a new value for i2
	   Log.d("IBM","instrumenting opcode: " +opcode + " - [ " + i1+ " , " + i2+" ]");
	   if (opcode == 159){ return i1==i2 ? i1+1 : i1;   } // IF_ICMPEQ 	0x9F
	   if (opcode == 160){ return i1!=i2 ? i1   : i1+1; } // IF_ICMPNE 	0xA0
	   if (opcode == 161){ return i1<i2  ? i1-1 : i1+1; } // IF_ICMPLT 	0xA1
	   if (opcode == 162){ return i1>=i2 ? i1+1 : i1-1; } // IF_ICMPGE 	0xA2
	   if (opcode == 163){ return i1>i2  ? i1+1 : i1-1; } // IF_ICMPGT 	0xA3
	   if (opcode == 164){ return i1<=i2 ? i1-1 : i1+1; } // IF_ICMPLE 	0xA4
	   if (opcode == 165){ return i1==i2 ? i1+1 : i1;   } // IF_ACMPEQ 	0xA5
	   if (opcode == 166){ return i1!=i2 ? i1   : i1+1; } // IF_ACMPNE 	0xA6
	   return i2;
	}
	
	public static long topLongToZero(long i) {
	       Log.d("IBM",String.format("Sleep Function: replacing long %d with a ZERO(0)", i));
	       return 0;
		}
}
